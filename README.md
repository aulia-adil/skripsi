# Implementasi Real Time Exam Monitoring pada Moodle (DEPRECATED, Jangan dijadikan acuan)

Streaming processing adalah pengolahan data secara real time. Dengan adanya sistem ini, programmer dapat melakukan deteksi anomali secara real time. Untuk mengimpelementasikannya, programmer dapat menggunakan Apache Kafka sebagai message queue.

Penulis mengambil ide skripsi real time exam monitoring karena kecurangan marak terjadi pada computer based test apalagi jika ujiannya dilakukan jarak jauh. Dengan ide ini, diharapkan dapat diimplementasikan machine learning untuk mendeteksi kecurangan sehingga dapat dicegah.

## Arsitektur Sistem

![sistem lama](gambar-arsitektur_sistem_lama.png)

Keterangan: Arsitektur sistem Scele sekarang



![Sistem Kak Favian](gambar-adil-dan-favian.png)

Keterangan: arsitektur sistem Adil dan Kak Favian



![Detail Sistem Kak Favian](gambar-favian_sistem_detail.png)

Keterangan: Detail arsitektur sistem Kak Favian

## Prerequisite Pemahaman

- Ip address dan port
- Terminal Linux
- Postgresql
- PHP dan JS untuk Moodle
- Instalasi Moodle dan Plugin-nya
- HTTP untuk komunikasi webserver
- Docker
- Google Cloud Platform Virtual Machine

## Cara Menjalankan Sistem

Telah dites di Google Cloud Platform VM dengan OS Ubuntu 18.04 dengan requirement komputer sebagai berikut:

- 8 GB RAM
- 2 Core
- 15 GB Memori

Karena masih dalam tahap prototype, maka firewall penulis matikan.

Berikut cara menjalankan sistem ini:

1. Clone repository ini `git clone https://gitlab.com/aulia-adil/skripsi.git`
2. Pindah ke directory-nya `cd skripsi`
3. Jalankan `bash bash/download_all_components.sh`
4. Jalankan `bash bash/install_favian_scele_datastream_system.sh`
5. Jalankan `bash bash/install_moodle_311.sh`
6. Jalankan `bash bash/install_asynchronous_plugin.sh`
7. Jalankan `bash bash/integrate_favian_and_adil_system.sh`
8. Buka Moodle:  http://public-ip-address
9. Masukan username: `admin` dan password: `Admin123*` pada saat login Moodle
10. Isilah administrasi pada Moodle
11. Buat quiz dengan peraturan wifiresilience
12. Lakukan suatu aktivitas pada quiz tersebut seperti pindah halaman, isi soal, dsb
13. Buka Grafana: http://public-ip-address:3000
14. Pada saat penulisan ini, Grafana memiliki username dan password default: admin
15. Add data source InfluxDB dengan URL `private-ip-address:8086` , database name: `t_integration_learninganalytics` , HTTP Method `GET`. Perlu diingat `private-ip-address` di sini **bukanlah** `localhost`.
16. Dengan menggunakan Grafana, lakukan query kepada influxDB: `SELECT * FROM "t-scele-log"` atau `SELECT * FROM "t-scele-asynchronous"`

## Ruang Lingkup / Jenis pekerjaan

Fokus skripsi penulis adalah melakukan input data untuk dilakukan stream processing. Data yang diinput pada quiz Moodle adalah sebagai berikut:

1. Jawaban soal + timestamp-nya
2. Flagging soal + timestamp-nya
3. Status online/offline + timestamp-nya
4. Aktivitas pindah halaman + timestamp-nya

## Rencana Pembuatan Aplikasi

Berikut rencana pengerjaan yang akan penulis buat:

#### Tahap 1: Implementasi AJAX (Easy)

- [x] Create database table for asynchronous feature
- [x] Create PHP script to serve asynchronous feature
- [x] Create asynchronous answer submission
- [x] Create asynchronous answer flagging
- [x] Create switch page activity tracker

#### Tahap 2: Implementasi Websocket (Medium)

- [x] Create online status tracker database column

- [x] Create online status tracker

#### Tahap 3: Menyambungkan Database dengan Sistem Kak Favian (Hard)

#### Tahap 4: Membuat UI/UX pada Asynchronous Plugin (Easy)

- [ ] Create feedback signal after submitting an answer
- [ ] Create status signal if online or offline

 #### Tahap 5: Melakukan Standardisasi Asynchronous Plugin (Hard)

## Acuan/ Pekerjaan sebelumnya

Dapat membaca hasil penelitian sebelumnya:

\- Skripsi Favian Kharisma Hazman

## Catatan

Sejauh yang penulis ketahui, belum ada algoritma machine learning yang mendeteksi kecurangan berdasarkan timestamp-nya. Oleh karena itu, hal ini bisa dijadikan ide penelitian.

## Related Repositories

- https://gitlab.com/aulia-adil/favian-warp-kafka.git
- https://gitlab.com/aulia-adil/favian-stacks-webserver.git
- https://gitlab.com/aulia-adil/asynchronous-plugin-reader.git
- https://gitlab.com/aulia-adil/favian-khodza.git
- https://gitlab.com/aulia-adil/moodle_plugin-asynchronous_submission.git

# Keterangan Istilah

- pr: pertemuan rutin

# My File Naming and Structure Guidelines

- Avoid special characters such as `` ~ ! @ # $ % ^ & * ( ) ` ; < > ? , [ ] { } ' "|``

  **Good:** `file.txt`

  **Bad**: `file([]).txt`

- Avoid whitespace and use hyphen instead

  **Good:** `file-name.txt`

  **Bad:** `file name.txt`

- Avoid uppercase and use lowercase

  **Good:** `file-name.txt`

  **Bad:** `File-Name.txt`

- Use underscore if a group of name has the same meaning

  **Good:** `muhammad_aulia_adil.txt`

  **Bad:** `MuhammadAuliaAdil.txt`

- File names should be short but descriptive (< 50 characters)

  **Good:** `cv-some_company-v1.pdf`

  **Bad:** `curriculum_vitae-bla_bla_bla...-end.pdf`

- Some rules can be violated for some files

  **Example:** `README.md`, `book_title_100%_win.pdf`

- Name a file as if it was a directory

  **Example:** `course-yymmdd-quiz-01.pdf`

- Directory name has the same rules as file name

- It is allowed to create a directory If it has more than **10 files** in it

- This project can only be 3 levels directory at most (this directory is level 1)
