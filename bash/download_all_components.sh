#!/bin/sh

sudo apt-get update

#Install semua tools umum
sudo apt-get install maven
sudo apt-get install vim
sudo apt-get install git

#Install Docker
sudo apt-get update
sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg     lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo service docker start
sudo usermod -a -G $(whoami)
sudo curl -L \
    https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#Install Postgresql
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ 'lsb_release -cs'-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib

#Install Moodle dependecies
sudo apt-get update
sudo apt -y install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install git
sudo apt install apache2 php7.4 libapache2-mod-php \
graphviz aspell ghostscript clamav php7.4-pspell php7.4-curl php7.4-gd \
php7.4-intl php7.4-pgsql php7.4-xml php7.4-xmlrpc php7.4-ldap php7.4-zip \
php7.4-soap php7.4-mbstring php-pgsql php-xml
sudo update-alternatives --set php /usr/bin/php7.4
sudo sed -i 's+;extension=pgsql+extension=pgsql+g' /etc/php/7.4/apache2/php.ini
sudo service apache2 restart
sudo apt-get update
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo apt-get install curl php-cli php-mbstring git unzip
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink(‘composer-setup.php’);"

#Download Moodle 311
cd
sudo git clone https://github.com/aulia-adil/moodle.git
cd moodle
sudo git branch --track MOODLE_311_STABLE origin/MOODLE_311_STABLE
sudo git checkout MOODLE_311_STABLE

#Download Asynchronous Plugin
cd
git clone https://gitlab.com/aulia-adil/moodle_plugin-asynchronous_submission.git

#Download Favian Scele Datastream System
cd
mkdir favian-scele-datastream-system
cd favian-scele-datastream-system
git clone https://gitlab.com/aulia-adil/favian-khodza.git
git clone https://gitlab.com/aulia-adil/favian-stacks-webserver.git
git clone https://gitlab.com/aulia-adil/favian-warp-kafka.git
git clone https://gitlab.com/aulia-adil/asynchronous-plugin-reader.git