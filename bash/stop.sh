#!/bin/sh

#Public ip address
SERVER=`hostname -I | cut -d' ' -f1`

sudo docker create network warp-net
sed -i "s/192.168.100.9/$SERVER/g" warp/docker-compose.yml
sudo docker-compose -f warp/docker-compose.yml down 
# sudo docker-compose -f stacks/postgresql/docker-compose.yml down 
sudo docker-compose -f stacks/debezium/docker-compose.yml down 
sudo docker-compose -f stacks/flink-cluster/docker-compose.yml down 
sudo docker-compose -f stacks/influx/docker-compose.yml down 
sudo docker-compose -f stacks/monitoring/docker-compose.yml down 
sudo docker-compose -f stacks/portainer/docker-compose.yml down 
sudo docker-compose -f stacks/elastic/docker-compose.yml down 

sed -i "s/192.168.100.9/$SERVER/g" stacks/debezium/refresh-connector.sh