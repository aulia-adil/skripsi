#!/bin/sh

sudo yum update -y
sudo yum install docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo curl -L \
    https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose version
sudo yum install maven
sudo yum install git

#!/bin/sh

sudo docker run -d \
--name moodle_docker \
 -p 5432:5432  \
 -e POSTGRES_USER=moodle_docker \
 -e POSTGRES_PASSWORD=moodle_docker \
 debezium/postgres:12

mkdir warp
cd warp
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/warp master
cd

mkdir stacks
cd stacks
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/stacks master
cd

mkdir khodza
cd khodza
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/khodza master

#!/bin/sh

cd
cd warp
docker network create warp-net
bash run_kafka_env.sh
cd

cd stacks
cd debeziun-infra
docker-compose up -d
cd ..
cd elastic
docker-compose up -d
cd ..
cd flink-cluster
docker-compose up -d
cd ..
cd influx
docker-compose up -d
cd ..
cd monitoring
docker-compose up -d

cd 
cd khodza
mvn clean package

#Stop all container
docker-compose down
docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -q)
