#!/bin/sh

cd /var/www/html/moodle/mod/quiz/accessrule
sudo cp -r ~/moodle_plugin-asynchronous_submission wifiresilience
cd /var/www/html/moodle/
sudo /usr/bin/php admin/cli/upgrade.php
sudo sed -i 's+"^1.6"+"^1.6","cboden/ratchet": "^0.4.3"+g' composer.json
sudo sed -i 's+true+true,"autoload":{"psr-4":{"MyApp\\\\":"src"}}+g' composer.json
sudo composer install
sudo composer update
sudo nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > \
~/websocket-output.log 2>&1 &