#!/bin/sh

bash stacks/debezium/refresh-connector.sh
sudo docker exec influxdb -execute  influx -execute "CREATE USER admin WITH PASSWORD 'Admin123*' WITH ALL PRIVILEGES"
sudo docker exec influxdb influx -execute "CREATE DATABASE t_integration_learninganalytics"

#For elastic search
sudo sysctl -w vm.max_map_count=262144