#!/bin/sh

#Variabel
dbhost=`hostname -I | cut -d' ' -f1`
dbport="5431"
dbname="postgres"
dbuser="moodleuser"
dbpass="moodleuser"

#Get public ip address
publicIpAddress=$(curl ifconfig.me)
wsport="5000"

# Instalasi Moodle
sudo cp -R ~/moodle /var/www/html/
cd /var/www/html/moodle/
sudo mkdir /var/moodledata
sudo chown -R www-data /var/moodledata
sudo chmod -R 777 /var/moodledata
sudo chmod -R 0755 /var/www/html/moodle
sudo chmod -R 777 /var/www/html/moodle
sudo chmod -R 0755 /var/www/html/moodle
sudo sed -i 's+/var/www/html+/var/www/html/moodle+g' /etc/apache2/sites-available/000-default.conf
sudo service apache2 restart
sudo cp config-dist.php config.php
sudo sed -i "0,/''/s//'$dbport'/" config.php
sudo sed -i "s+'localhost';+'$dbhost';+g" config.php
sudo sed -i "s+'moodle';+'$dbname';+g" config.php
sudo sed -i "s+'username';+'$dbuser';+g" config.php
sudo sed -i "s+'password';+'$dbpass';+g" config.php
sudo sed -i "s+example.com/moodle';+$publicIpAddress';\n\$CFG->wsroot = 'ws://$publicIpAddress';\n\$CFG->wsport = $wsport;+g" config.php
sudo sed -i "s+/home/example/moodledata+/var/moodledata+g" config.php
sudo /usr/bin/php admin/cli/install_database.php --adminuser=admin --adminpass=Admin123* --agree-license