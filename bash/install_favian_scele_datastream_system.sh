#!/bin/sh

# Public ip address
SERVER=`hostname -I | cut -d' ' -f1`

# Compile khodza and asynchronous-plugin-reader
cd ~/favian-scele-datastream-system
sed -i "s/192.168.43.220/$SERVER/g" favian-khodza/src/main/resources/environment.properties
sed -i "s/192.168.43.220/$SERVER/g" asynchronous-plugin-reader/src/main/resources/environment.properties
sed -i "s/192.168.43.220/$SERVER/g" asynchronous-plugin-reader/src/main/java/org/datastream/khodza/sink/influx/AsynchronousInfluxMap.java
cd ~/favian-scele-datastream-system/favian-khodza/
mvn clean package
cd ~/favian-scele-datastream-system/asynchronous-plugin-reader/
mvn clean package

# Replace some configurations
cd ~/favian-scele-datastream-system
sed -i "s/192.168.43.220/$SERVER/g" favian-warp-kafka/docker-compose.yml

# Install webservers
cd ~/favian-scele-datastream-system/
sudo docker network create warp-net
sudo docker-compose -f favian-warp-kafka/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/postgresql/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/debezium/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/flink-cluster/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/influx/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/monitoring/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/portainer/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/elastic/docker-compose.yml up -d
