#/!bin/sh

# Make Debezium as a bridge between Kafka and Postgresql
bash ~/favian-scele-datastream-system/favian-stacks-webserver/debezium/refresh-connector.sh

# Create database in influxdb
sudo docker exec influxdb influx -execute "CREATE DATABASE t_integration_learninganalytics"

# For elastic search
sudo sysctl -w vm.max_map_count=262144

# Run flink jobs
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 asynchronous.jar org.datastream.khodza.job.Scele scele.s1.properties
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 khodza.jar org.datastream.khodza.job.Scele scele.s1.properties