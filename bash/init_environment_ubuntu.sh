#!/bin/sh

#Instalasi Dockerr
sudo apt-get update
sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg     lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo service docker start
sudo usermod -a -G $(whoami) 

sudo curl -L \
    https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo apt-get install maven
sudo apt-get install vim
sudo apt-get install git

#Install postgresql
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ 'lsb_release -cs'-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib 

#Install postgresql debezium
sudo docker run -d \
--name moodle_docker \
 --restart=always \
 -p 5431:5432  \
 -e POSTGRES_USER=moodle_docker \
 -e POSTGRES_PASSWORD=moodle_docker \
 debezium/postgres:12

#Install kowl
sudo docker run --name=warp-kowl --restart=always -d --network=warp-net -p 8091:8080 -e KAFKA_BROKERS=kafka-1:9092 quay.io/cloudhut/kowl:master

#Install portainer
sudo docker run --name=portainer --restart=always -d -p 8000:8000 -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

#Download webservers
mkdir warp
cd warp
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/warp master

#Change ip address
sed -i 's/${SERVER_IP}/10.128.0.2/g' docker-compose.yml

sudo docker create network warp-net
sudo docker-compose up -d
cd

mkdir khodza
cd khodza
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/khodza master
cd

mkdir stacks
cd stacks
git init
git config user.email "muhammad.aulia93@ui.ac.id"
git config user.name "muhammad.aulia93"
git pull https://muhammad.aulia93:9R87McpLNZ@gitlab.cs.ui.ac.id/csui-datastream/stacks master
cd

#Install stacks
cd stacks
cd debeziun-infra
sudo docker-compose up -d
cd ..
cd elastic
sudo docker-compose up -d
cd ..
cd flink-cluster
sed -i 's/${FLINK_DOCKER_IMAGE_NAME:-flink}/flink:1.10.0-scala_2.11/g' docker-compose.yml
sudo docker-compose up -d
cd ..
cd influx
sed -i 's/latest/1.8.0/g' docker-compose.yml
sudo docker-compose up -d
cd ..
cd monitoring
sudo docker-compose up -d

cd 
cd khodza
mvn clean package

#Change configuration debezium
sed -i 's/10.119.105.71/localhost/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/152.118.29.234/10.128.0.2/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/iconsent/moodle_docker/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/scele_s1/moodle_docker/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/dbzium/moodle_docker/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/5432/5431/g' ~/stacks/debeziun-infra/refresh-connector.sh
sed -i 's/public.mdl_user/public.mdl_user,public.mdl_asynchronous/g' ~/stacks/debeziun-infra/refresh-connector.sh

#Setting influxdb
sudo docker exec influxdb influx -execute "create user admin with password 'Admin123*' with all privileges"
sudo docker exec influxdb influx -execute "CREATE DATABASE t_integration_learninganalytics"

sudo docker exec flink-cluster_jobmanager_1 ./bin/flink run --detached -c org.datastream.khodza.Khodza khodza.jar org.datastream.khodza.job.Scele scele.s1.properties
#Configure elastic search
sudo sysctl -w vm.max_map_count=262144

#Shortcut for influxdb
#influx
#use t_integration_learninganalytics
#show measurements
#select * from "t-scele-log"
#35.223.240.206:9092/topics/p-scele.public.mdl_asynchronous
#curl -XPOST "35.223.240.206:9092/consumers/1"

#Configure monitoring
#Ada yang harus diganti yml file-nya supaya mirip sama yang ada di lokal adil

#Argumen flink
#org.datastream.khodza.Khodza
#org.datastream.khodza.job.Scele scele.s1.properties
#./bin/flink run --detached -c org.datastream.khodza.Khodza khodza.jar org.datastream.khodza.job.Scele scele.s1.properties
#./bin/flink run-application -c org.datastream.khodza.Khodza khodza.jar org.datastream.khodza.job.Scele scele.s1.properties

