#!/bin/sh

# Variabel
dbhost=`hostname -I | cut -d' ' -f1`
dbport="5431"
dbname="postgres"
dbuser="moodleuser"
dbpass="moodleuser"

# Get public ip address
publicIpAddress=$(curl ifconfig.me)
wsport="5000"

# Reupdate config.php
cd /var/www/html/moodle/
sudo cp config-dist.php config.php
sudo sed -i "0,/''/s//'$dbport'/" config.php
sudo sed -i "s+'localhost';+'$dbhost';+g" config.php
sudo sed -i "s+'moodle';+'$dbname';+g" config.php
sudo sed -i "s+'username';+'$dbuser';+g" config.php
sudo sed -i "s+'password';+'$dbpass';+g" config.php
sudo sed -i "s+example.com/moodle';+$publicIpAddress';\n\$CFG->wsroot = 'ws://$publicIpAddress';\n\$CFG->wsport = $wsport;+g" /var/www/html/moodle/config.php
sudo sed -i "s+/home/example/moodledata+/var/moodledata+g" config.php

# Run websocket
sudo nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > \
~/websocket-output.log 2>&1 &

# For elastic search
sudo sysctl -w vm.max_map_count=262144

# Make Debezium as a bridge between Kafka and Postgresql
bash ~/favian-scele-datastream-system/favian-stacks-webserver/debezium/refresh-connector.sh

# To run flink job
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 asynchronous.jar org.datastream.khodza.job.Scele scele.s1.properties
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 khodza.jar org.datastream.khodza.job.Scele scele.s1.properties